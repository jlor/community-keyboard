<?php

$key = $_GET['key'];

$message = 
'<span id="'.$key.'" class="selectedKey"/><h2>Key: '.$key.'</h2>';


function key_taken($key) {
    include('sql.php');
    $stmt = $db->query("SELECT `key` FROM `users` WHERE `key`='".$key."';");
    $result = $stmt->fetch();
    return ($result["key"] == $key);
}


if (key_taken($key)) {
$message .= '<strong><font color="red">Key already reserved</font></strong>
<h3>Please choose another key</h3>
<p>If this is yours and you wish to cancel, fill in the below form:</p>
<form class="form-inline" role="form>
  <div class="form-group">
    <div class="input-group">
      <label class="sr-only" for="deleteEmail">Reddit username:</label>
      <input class="form-control" id="deleteEmail" placeholder="Reddit username">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <label class="sr-only" for="deletePassword">Password</label>
      <input type="password" class="form-control" id="deletePassword" placeholder="Password">
    </div>
  </div>
  <button id="delete" type="submit" class="btn btn-default">Submit</button>
</form>';
} else {
$message .= '<form class="form-inline" role="form">
  <div class="form-group">
    <div class="input-group">
      <label for="reserveEmail">Reddit username:</label>
      <input class="form-control" id="reserveEmail" placeholder="Reddit username">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <label for="reservePassword">Password</label>
      <input type="password" class="form-control" id="reservePassword" placeholder="Password">
    </div>
  </div>
  <button id="reserve" type="submit" class="btn btn-default">Submit</button>
</form>';
}
header('Content-Type: application/json');
echo (json_encode(array('message' => $message)));
?>
