<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Collection and status site for reddit community keyboard">
        <meta name="keywords" content="Reddit, Keyboard, ANSI, community, 8bitderp">
        <meta name="author" content="Jakob Rosenlund and 8bitderp">

        <title>ANSI 104 community keyboard!</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

        <style>
.key {
stroke: black;
fill: white;
}

.key:hover {
fill: gray;
}
.d3-tip {
  line-height: 1;
  font-weight: bold;
  padding: 12px;
  background: rgba(0, 0, 0, 0.8);
  color: #fff;
  border-radius: 2px;
}

/* Creates a small triangle extender for the tooltip */
.d3-tip:after {
  box-sizing: border-box;
  display: inline;
  font-size: 10px;
  width: 100%;
  line-height: 1;
  color: rgba(0, 0, 0, 0.8);
  content: "\25BC";
  position: absolute;
  text-align: center;
}

/* Style northward tooltips differently */
.d3-tip.n:after {
  margin: -1px 0 0 0;
  top: 100%;
  left: 0;
}

        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
<div class="col-xs-12">
<h1>Please click the key you want to provide to the community keyboard!</h1>
</div>
            </row>
            <div class="row">
                <div class="col-xs-12">
                    <?php include_once('keyboard-rect.svg'); ?>

                </div>
            </div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- d3.js stuff -->
<script src="d3.v3.min.js"></script>
<script src="d3.tip.v0.6.3.js"></script>


<script type="text/javascript">
// Handle popovers of keys
var tip = d3.tip()
.attr('class', 'd3-tip')
.html(function(d) {
    $.ajax({
        type: "GET",
            url: 'form.php',
            dataType: 'json',
            data: {key: $(this).attr('id')},
            success: function(data) {
                $('.d3-tip').html(data.message);

                // Handle form control in popovers
                $("#reserve").click(function(event) {
                    $.ajax({
                        type: "GET",
                            url: 'reserve.php',
                            dataType: 'json',
                            data: {action: 'reserve',
                                   key: $('.selectedKey').attr('id'),
                                   email: $('#reserveEmail').val(),
                                   password: $('#reservePassword').val()},
                            success: function(data) {
                                if (data.error.length > 0) { alert("ERROR: " + data.error); }
                                else { alert(data.message); }
                            }
                    });
                    event.preventDefault();
                });

                $("#delete").click(function(event) {
                    $.ajax({
                        type: "GET",
                            url: 'reserve.php',
                            dataType: 'json',
                            data: {action: 'delete',
                                   key: $('.selectedKey').attr('id'),
                                   email: $('#deleteEmail').val(),
                                   password: $('#deletePassword').val()},
                            success: function(data) {
                                if (data.error.length > 0) { alert("ERROR: " + data.error); }
                                else { alert(data.message); }
                            }
                    });
                    event.preventDefault();
                });
            }
    });
});

var svg = d3.select("body").select("svg");

svg.call(tip);

svg.selectAll(".key")
    .on('click', tip.show);



</script>
    </body>
</html>
